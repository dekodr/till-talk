<?php defined('BASEPATH') OR exit('No direct script access allowed');

class analytic extends CI_Controller {

	public $figure;

	public function __construct(){
		error_reporting(E_ERROR);
		parent::__construct();

		$this->figure_1 = array('WHEAT FLOUR', 'BABY SHAMPOO', 'BABY SOAP', 'SHAMPOO', 'BABY BALM', 'BABY SKIN CARE', 'BABY FOOD', 'BABY TISSUE', 'BABY DIAPERS', 'ORAL CARE', 'SOY SAUSCE', 'COTTON', 'SOAP', 'DETERGENT', 'TISSUE', 'CREAM SOAP', 'CLOTHES SOFTENER', 'PORCLAIN CLEANER', 'CAMPHOR', 'CLOTHES WHITENER', 'INSECTISIDE', 'DISHWASHER SOAP', 'BAG TEA', 'POWDER MILK', 'POWDER COFFEE','CAN FOOD', 'INSTANT NOODLE', 'COCONUT MILK','INSTANT SEASONING', 'IRON SPRAY', 'MARGARINE', 'MOSQUITO LOTION', 'RICE', 'SUGAR', 'CHEESE');
		$this->figure_2 = array('WHEAT FLOUR', 'SHAMPOO', 'ORAL CARE', 'SOY SAUSCE', 'COTTON', 'SOAP', 'DETERGENT', 'TISSUE', 'CREAM SOAP', 'CLOTHES SOFTENER', 'PORCLAIN CLEANER', 'CAMPHOR', 'CLOTHES WHITENER', 'INSECTISIDE', 'DISHWASHER SOAP');
		$this->figure_3 = array('WHEAT FLOUR', 'BABY SHAMPOO', 'BABY SOAP', 'SHAMPOO', 'BABY BALM', 'BABY SKIN CARE', 'BABY FOOD', 'BABY TISSUE', 'BABY DIAPERS', 'ORAL CARE', 'SOY SAUSCE', 'COTTON', 'SOAP', 'DETERGENT', 'TISSUE', 'CREAM SOAP', 'CLOTHES SOFTENER', 'PORCLAIN CLEANER', 'CAMPHOR', 'CLOTHES WHITENER', 'INSECTISIDE', 'DISHWASHER SOAP');
		$this->figure_4 = array('WOMAN SHAMPOO', 'WOMAN DIAPERS', 'WOMAN FACIAL CARE', 'WOMAN SKIN CARE', 'ORAL CARE', 'COTTON', 'SOAP', 'DETERGENT', 'TISSUE', 'CAMPHOR', 'INSECTISIDE', 'DISHWASHER SOAP', 'SNACKS & BISCUITS', 'INSTANT NOODLE', 'MOSQUITO LOTION', 'COMMON COLD');		
		$this->figure_5 = array('DEODORANT', 'MEN PERSONAL CARE', 'MEN SHAMPOO', 'MEN FACIAL CARE');
		$this->figure_6 = array('RICE', 'WHEAT FLOUR', 'EGG', 'MINERAL WATER', 'POWDER JELLY', 'BREAD');
		$this->figure_7 = array('KRETEK CIGARETES', 'BOX MILK', 'DIARY & ICE CREAM', 'SNACKS & BISCUITS');

	}

	function get_index_of($name = '', $figure = ''){
		foreach($figure as $index => $fig){
			if($fig == $name) return $index;
		}
	}

	public function get_shopping_list($id_customer = ''){
		$return = array();

		$sql = "SELECT * FROM tr_shop_master WHERE id_member = ?";
		$sql = $this->db->query($sql, array($id_customer));
		

		foreach($sql->result() as $data){
			$array = array();

			$_sql = "SELECT a.*, 
							b.sub_category,
							c.figure 

							FROM tr_shop_detail a 
							LEFT JOIN ms_product b ON b.name = a.product_name 
							LEFT JOIN tr_shop_master c ON c.id = a.id_master 

							WHERE a.id_master = ?
							ORDER BY a.id ASC";

			$_sql = $this->db->query($_sql, array($data->id));	

			$array['date'] = $data->timestamp;
			$array['kasir'] = $data->id_kasir;
			$array['data'] = array();

			foreach($_sql->result() as $_data){
				$fig = "figure_".$_data->figure;
				$fig = $this->{$fig};

				$index = $this->get_index_of($_data->sub_category, $fig);
				$array['data'][$index] = array('qty' => $_data->qty, 'id' => $_data->product_name);
			}

			array_push($return, $array);
		}

		return array('fig' => $_data->figure, 'data' => $return);
	}

	public function get_result($id_customer = ''){
		$list = $this->get_shopping_list($id_customer);

		$return = $list['data'];
		$fig = "figure_".$list['fig'];
		$fig = $this->{$fig};

		$summary = $promo = array();

		foreach($fig as $index => $figure){
			foreach($return as $data){
				$data = $data['data'];

				if($data[$index]){
					$result[$index][$data[$index]['id']]++;
				}
			}
		}

		foreach($result as $_index => $data){
			$max = $total = 0;
			$product = "";

			foreach($data as $index => $_data){
				if($_data >= $max){ $max = $_data; $product = $index; } 
				$total += $_data;
			}	

			$loyalty = round((100 / $total) * $max);

			if($loyalty <= 50){
				$status = "undecided";
				array_push($promo, $fig[$_index]);
			}
			if($loyalty > 50 and $loyalty < 80)
				$status = "like the product, but trying another";
			if($loyalty >= 80)
				$status = "solid";

			array_push($summary, array(
				'category' => $fig[$_index],
				'top-brand' => $product,
				'purchses' => $max." purchases of ".$total,
				'loyalty' => $loyalty."%",
				'status' => $status
				//dwell variant
			));
			
			$loyalty_score += $loyalty;  
		}

		$visit_total = count($fig);
		$overall_loyalty = round($loyalty_score / $visit_total);

		if($overall_loyalty <= 50)
			$status = "undecided buyer, can target any promo";
		if($overall_loyalty > 50 and $overall_loyalty < 80)
			$status = "likely promo hunt, or customer run product-test for various brand";
		if($overall_loyalty >= 80)
			$status = "solid buyer";

		$list = "\n\n";
		$x = 1;

		foreach($summary as $data){
			$message = $x.'. '.$data['top-brand']."\n";
			
			if($data['loyalty'] <= 50)
				$message .= "   [put promo here]\n\n";

			//array_push($list, $message);
			$list .= $message;
			$x++;
		}

		$grand_result = array(
			'brands-info-by-category' => $summary,
			'customer-overall-loyalty-score' => $overall_loyalty,
			'customer-summary' => $status,
			'strong-possibility-of-promo' => $promo,
			'suggested-next-shopping-list' =>$list
		);


		//die(json_encode($grand_result));
		return($grand_result);
	}		

}
