<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {
	public $form;
	public $module = 'API';
	public function __construct(){
		parent::__construct();
		$this->load->model('Daily_model','dm');
	}
	public function index(){
		$data['daily'] = $this->dm->getData();
		$this->content = $this->load->view('daily/daily',$data, TRUE);

		parent::index();
	}
	public function cart($id){
		echo json_encode($this->dm->getDaily($id));
	}
	public function cartDetail($id){
		echo json_encode($this->dm->getCartDetail($id));
	}

	public function getBeacon(){

		$handle = fopen("log/log.txt", 'a') or die('Cannot open file');
		$_GET['timestamp'] = date("Y-m-d H:i:s");

		$data = 'Minor : '.$_GET['minor'].' | timestamp : '.$_GET['timestamp'].' | status : '.$_GET['status'] ."\n";
		fwrite($handle, $data);

		fclose($handle);

		
	}
}