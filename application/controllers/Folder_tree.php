<?php defined('BASEPATH') OR exit('No direct script access allowed');

class folder_tree extends CI_Controller {

	public function __construct(){
        parent::__construct();
	        date_default_timezone_set('Asia/Jakarta');
			$this->load->model('Main_model', 'mm');
	}

	public function index(){
		$data['member']			= $this->mm->get_list_member();

		$layout['title'] 		= 'Tilltalk-Dashboard';
		$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
		$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
		$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
		$layout['contentMenu'] 	= $this->load->view('folder-tree/index',$data,TRUE);
		$this->load->view('template/template',$layout);		
	}

	function member($id_member ="", $date = ""){
		if($date ==""){
			$data['user']			= $this->mm->get_member($id_member);
			$data['shop']			= $this->mm->get_shop_member($id_member, $date);
			//  print_r($data);
			$layout['title'] 		= 'Tilltalk-Dashboard';
			$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
			$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
			$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
			$layout['contentMenu'] 	= $this->load->view('folder-tree/index',$data,TRUE);
			$this->load->view('template/template',$layout);

		}else if($date !==""){
			$data['date']			= $date;
			$data['user']			= $this->mm->get_member($id_member);
			$data['shop']			= $this->mm->get_shop_member($id_member, $date);
			//  print_r($data);

			$layout['title'] 		= 'Tilltalk - '.$data['user']['name'];
			$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
			$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
			$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
			$layout['contentMenu'] 	= $this->load->view('folder-tree/detail',$data,TRUE);
			$this->load->view('template/template',$layout);
		}
	}

	/*==============================================
					NEWS & EVENT
	==============================================*/
}