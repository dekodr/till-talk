<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	public function __construct() {
        parent::__construct();
	        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Main_model', 'mm');
	}

	public function index(){

		$data						= $this->mm->get_total_data();
		$data['data_category']		= $this->mm->get_data_category();
		$data['data_area']			= $this->mm->get_data_area();
		// print_r($data);
		$layout['title'] 		= 'Tilltalk-Dashboard';
		$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
		$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
		$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
		$layout['contentMenu'] 	= $this->load->view('template/dashboard-home',$data,TRUE);
		$layout['script']	 	= $this->load->view('template/dashboard-home_js',$data,TRUE);
		$this->load->view('template/template',$layout);
	}

}