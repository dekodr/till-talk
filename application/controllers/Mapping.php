<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mapping extends CI_Controller {

	public function __construct() {
        parent::__construct();
	        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Mapping_model', 'mpm');
	}

	public function member($id_member="", $id_shop_master=""){
		$this->dekodr($id_member, $id_shop_master);
	}

	public function dekodr($id_member="", $id_shop_master=""){

		$data['data_category']		= $this->mpm->get_subcategory();
		$data['cor']				= $this->mpm->get_path($id_member, $id_shop_master);

		$layout['title'] 		= 'Tilltalk-Dashboard Mapping';
		$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
		$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
		$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
		$layout['contentMenu'] 	= $this->load->view('mapping/dekodr',$data,TRUE);
		$layout['script']	 	= $this->load->view('mapping/dekodr_js',$data,TRUE);
		$this->load->view('template/template',$layout);
	}

	public function master($id){
		
		$data['id']                	= $id;
		$data['data_category']		= $this->mpm->get_subcategory();
		$data['cor']				= $this->mpm->get_master($id);
        // print_r($data);
		$layout['title'] 		= 'Tilltalk-Dashboard Map DEKODR';
		$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
		$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
		$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
		$layout['contentMenu'] 	= $this->load->view('mapping/index',$data,TRUE);
		$layout['script']	 	= $this->load->view('mapping/index_js',$data,TRUE);
		$this->load->view('template/template',$layout);
	}

}