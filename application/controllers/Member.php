<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__)."/Analytic.php");
class member extends analytic {

	public function __construct(){
        parent::__construct();
	        date_default_timezone_set('Asia/Jakarta');
                $this->load->model('Main_model', 'mm'); 
	}

	public function index(){
		$data['member']			= $this->mm->get_list_member();

		$layout['title'] 		= 'Tilltalk-Dashboard';
		$layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
		$layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
		$layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
		$layout['contentMenu'] 	= $this->load->view('member/index',$data,TRUE);
		$this->load->view('template/template',$layout);		
	}

	function detail($id_member =""){
                $data['user']			= $this->mm->get_member($id_member);
                $data['data']			= $this->mm->get_member_data($id_member);
                // print_r($data);
                
                $layout['title'] 		= 'User - '.$data['user']['name'];
                $layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
                $layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
                $layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
                $layout['contentMenu'] 	= $this->load->view('member/detail',$data,TRUE);
                $this->load->view('template/template',$layout);
	}

	function category($id_member =""){
                $data['user']			= $this->mm->get_member($id_member);
                $data['data']			= $this->mm->get_member_data($id_member);
                $data['data_category']		= $this->mm->get_data_category();
                $data['analitycs']              = $this->get_result($id_member);
                // print_r($data);
                
                $layout['title'] 	= 'User - '.$data['user']['name'];
                $layout['userInfo'] 	= $this->load->view('template/user-info',null,TRUE);
                $layout['navbarMenu'] 	= $this->load->view('template/navbar-menu',null,TRUE);
                $layout['sideMenu'] 	= $this->load->view('template/side-menu',null,TRUE);
                $layout['contentMenu'] 	= $this->load->view('member/category',$data,TRUE);
                $this->load->view('template/template',$layout);
        }
        
        
}