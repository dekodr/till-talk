<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retail extends CI_Controller{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('retail_model');
	}
	
	public function index(){
		$this->load->view('retail/main');
	}

	public function transaction(){
		$post = $this->input->post();
		$data['is_active'] = 0;

		if($post){
			$post['barcode'] = str_replace('`', '0', $post['barcode']);
			$query = $this->retail_model->get_product($post['barcode']);
	
			$session = $this->session->userdata();
			$session = $session['list'];

			if(!$session){ $this->session->set_userdata('list', array()); $session = array(); }
			

			array_push($session, array('name' => $query['name'], 'qty' => 1, 'price' => $query['price']));
			$this->session->set_userdata('list', $session);
			
			$data['is_active'] = 1;
		}

		$session = $this->session->userdata();
		$data['session'] = $session['list'];

		$this->load->view('retail/transaction', $data);
	}

	public function batal(){
		$this->session->sess_destroy();
		echo '<script type="text/javascript">location.replace("'.base_url().'retail");</script>';
	}

	public function finish(){
		
		$session = $this->session->userdata();
		$session = $session['list'];

		$id = $this->retail_model->save_master(array(
			"id_kasir" => 2, 
			"id_member" => 0,
			"timestamp" => date("Y-m-d H:i:s")
		));

		foreach($session as $data){
			$this->retail_model->save_detail(array(
				"id_master" => $id, 
				"product_name" => $data['name'],
				"qty" =>$data['qty'],
				"entry_stamp" => date("Y-m-d H:i:s")
			));
		}

		$this->session->sess_destroy();
		echo '<script type="text/javascript">location.replace("'.base_url().'retail");</script>';
	}
}