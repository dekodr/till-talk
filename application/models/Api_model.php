<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Api_model extends CI_Model{

	function __construct(){

		parent::__construct();

	}


	function get_total_data(){
		$data['item']		= $this->db->select('count(id) as total')->get('ms_product')->row_array();
		$data['cat']		= $this->db->select('count(sub_category) as total')->get('ms_product')->row_array();
		$data['cust']		= $this->db->select('count(id) as total')->get('ms_member')->row_array();
		//$data['promo']		= $this->db->select('count(discount) as total')->where('discount > 0')->get('saleitems')->row_array();
		$data['top_list']	= $this->top_list(10);

		// print_r($data);die;
		return $data;
	}

}