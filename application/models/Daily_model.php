<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daily_model extends CI_Model{
	public $table = 'tr_shop_master';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$member = $this->db->query("SELECT * FROM ms_member")->result_array();
		$return = array();
		foreach ($member as $key => $value) {

			$master = "	SELECT
						*
						FROM ".$this->table." a LEFT JOIN ms_member b ON a.id_member = b.id WHERE id_member = ?";
			$master = $this->db->query($master, array($value['id']))->result_array();
			$return[$key] = $value;
			foreach($master as $keyMaster => $valueMaster){
				$return[$key]['master'][$keyMaster] = $valueMaster;
				$return[$key]['master'][$keyMaster]['detail'] = $this->getDetail($value['id']);
			}
			
		}
		return $return;
	}
	function getDataMember($form){

		$query = "	SELECT
						*

					FROM ".$this->table." a WHERE id_member IS NOT NULL ";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function getDetail($id){
		$query = $this->db->where('id_master', $id)->get('tr_shop_detail');
		return $query->result_array();
	}
	function getDetailMember($id){
		$query = "SELECT a.* from tr_shop_detail a JOIN tr_shop_master b ON a.id_master = b.id WHERE b.id_member = ?";
		$query = $this->db->query($query, array($id));
		return $query->result_array();
	}
	function getDaily($id){
		$query = "SELECT * FROM tr_shop_master WHERE id_member = ?";
		$query = $this->db->query($query, array($id));
		// echo print_r($query->result_array())

		return $query->result_array();
	}
	function getCartDetail($id){
		$query = "SELECT * FROM tr_shop_detail WHERE id_master = ?";
		$query = $this->db->query($query, array($id));
		return $query->result_array();
	}

}
