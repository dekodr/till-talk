<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Main_model extends CI_Model{

	function __construct(){

		parent::__construct();

	}


	function get_total_data(){
		$data['item']		= $this->db->select('count(id) as total')->get('ms_product')->row_array();
		$data['cat']		= $this->db->select('count(sub_category) as total')->get('ms_product')->row_array();
		$data['cust']		= $this->db->select('count(id) as total')->get('ms_member')->row_array();
		//$data['promo']		= $this->db->select('count(discount) as total')->where('discount > 0')->get('saleitems')->row_array();
		$data['top_list']	= $this->top_list(10);

		// print_r($data);die;
		return $data;
	}

	function get_data_category(){
		$data = $this->top_category();

		return ($data);
	}

	function get_data_area(){
		$loc	= $this->db->get('ms_store')->result_array();

		foreach ($loc as $key => $value) {
			for ($i=1; $i <= 12; $i++) { 
				$data[$value['store_location']][$i] = $this->db->select('count(qty) as total')
													->join('tr_shop_master', 'tr_shop_master.id = tr_shop_detail.id_master')
													->join('ms_cashier', 'ms_cashier.id = tr_shop_master.id_kasir')
													->join('ms_store', 'ms_store.id = ms_cashier.id_store')
													->where('ms_store.id', $value['id'])
													->where('MONTH(tr_shop_master.timestamp)', $i)
													->order_by('total', 'DESC')
													->get('tr_shop_detail')->row_array();
													// print_r($this->db->lastsql());
			}
		}
		foreach ($data as $loc_ => $value) {
			$data_[$loc_]['name'] = $loc_;
			$data_[$loc_]['data'] = array();
			foreach ($value as $key_ => $value_) {
				# code...
				array_push($data_[$loc_]['data'], $value_['total']);
			}
		}
		// print_r($data_);die;
		return $data_;
	}

	function top_list($limit = ""){
		$sql	= $this->db->select('product_name, sub_category, count(qty) as total')
								->join('ms_product', 'ms_product.name = tr_shop_detail.product_name')
								->group_by('product_name')
								->order_by('total', 'DESC');
		if($limit > 0){
			$sql 	= $this->db->limit($limit);
		}
		
		$sql = $this->db->get('tr_shop_detail');
		return $sql->result_array();
	}
	
	function top_category($limit = ""){
		$sql	= $this->db->select('category as name, count(qty) as y')
								->join('ms_product', 'ms_product.name = tr_shop_detail.product_name')
								->group_by('category');
								
		$sql = $this->db->get('tr_shop_detail');
		return $sql->result_array();
	}

	function get_list_member(){
		return $this->db->get('ms_member')->result_array();
	}

	function get_member($id_member=""){
		return $this->db->where('id', $id_member)->get('ms_member')->row_array();
	}
	
	function get_shop_member($id_member="", $date=""){
		$data = $this->db
						->select('id_master, id_kasir, tr_shop_master.timestamp, product_name, qty, price, ms_store.store_name, ms_store.store_location')
						->join('tr_shop_detail', 'tr_shop_detail.id_master = tr_shop_master.id')
						->join('ms_store', 'ms_store.id = tr_shop_master.id_kasir', "LEFT")
						->where('tr_shop_master.id_member', $id_member)
						->get('tr_shop_master')->result_array();

		foreach($data as $key => $value){
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['id_kasir'] 		= $value['id_kasir'];
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['store_name'] 		= $value['store_name'];			
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['store_location'] 	= $value['store_location'];
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['product_name'] 	= $value['product_name'];
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['qty'] 				= $value['qty'];
			$return[date('M-Y', strtotime($value['timestamp']))][$value['timestamp']][$key]['price'] 			= $value['price'];
		}

		if($date !==""){			
			$return = $this->db
						->select('id_master, category, id_kasir, tr_shop_master.timestamp, product_name, qty, tr_shop_detail.price, tr_shop_detail.timestamp AS entry_stamp, store_name, store_location')
						->join('tr_shop_detail', 'tr_shop_detail.id_master = tr_shop_master.id')
						->join('ms_product', 'ms_product.name = tr_shop_detail.product_name', "LEFT")
						->join('ms_store', 'ms_store.id = tr_shop_master.id_kasir', "LEFT")
						->where('tr_shop_master.id_member', $id_member)
						->like('tr_shop_detail.timestamp', $date)
						->get('tr_shop_master')->result_array();
		}

		// print_r($return);die;
		return $return;
	}

	function get_shop_per_member($id_member="", $date=""){
		$data = $this->db->where('id_member', $id_member)->get('tr_shop_master')->result_array();

		// print_r($data);die;
		return $data;
	}

	
	function get_member_data($id_member=""){
		$shop = $this->db
						->select('id_master, id_kasir, tr_shop_master.timestamp, product_name, qty, price')
						->join('tr_shop_detail', 'tr_shop_detail.id_master = tr_shop_master.id')
						->where('tr_shop_master.id_member', $id_member)
						->get('tr_shop_master')->result_array();
		$cat = $this->db
						->select('category')
						->join('tr_shop_detail', 'tr_shop_detail.id_master = tr_shop_master.id')
						->join('ms_product', 'ms_product.name = tr_shop_detail.product_name')
						->group_by('category')
						->where('tr_shop_master.id_member', $id_member)
						->get('tr_shop_master')->result_array();
		
		$data['shops'] = $this->db->where('tr_shop_master.id_member', $id_member)->get('tr_shop_master')->result_array();
		$data['total'] = 0; 
		
		foreach($shop as $key => $value){
			$data['total'] += $value['price']; 
		}
		
		$data['shops'] = count($data['shops']);
		$data['cat'] = count($cat);

		// print_r($data);
		
		return $data;
	}
}