<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Mapping_model extends CI_Model{

	function __construct(){

		parent::__construct();

	}


    function get_subcategory(){
        return $this->db->select('category, sub_category')
                    ->order_by('category', 'ASC')
                    ->order_by('sub_category', 'ASC')
                    ->group_by('sub_category')
                    ->get('ms_product')
                    ->result_array();
    }

    function get_master($id){
        $data = $this->db->select('group_concat(sub_category) AS sub_category')
        ->where('tr_shop_detail.id_master', $id)
        ->join('ms_product', 'ms_product.name = tr_shop_detail.product_name')
        // ->group_by('sub_category')
        ->get('tr_shop_detail')
        ->row_array();
        // print_r($data);

        return $data['sub_category'];
    }

    function get_path($id_member = '', $id_shop_master = ""){
        $beacon_path    = $this->db->select('ms_beacon.sub_category as name, status, time')
                            ->join('ms_beacon', 'ms_beacon.id = tr_beacon_path.id_beacon')
                            ->where('tr_beacon_path.id_member', $id_member)
                            ->where('tr_beacon_path.id_shop_master', $id_shop_master)
                            ->where('tr_beacon_path.del', 0)
                            ->order_by('time')
                            ->get('tr_beacon_path')->result_array();
        $path_line      = array();
            foreach ($beacon_path as $key => $value) {
                $path_line[$value['name']][$value['status']]['name']  = $value['name'];
                $path_line[$value['name']][$value['status']]['status']  = $value['status'];
                $path_line[$value['name']][$value['status']]['time']  = $value['time'];
            }
            foreach ($path_line as $key => $value) {
                # code...
                foreach ($value as $key_ => $value_) {
                    $path_line_[$value_['name']]['name']  = $value_['name'];
                    $path_line_[$value_['name']]['time'][ $value_['status']]  = $value_['time'];
                }
            }
            foreach ($path_line_ as $name => $value) {
                $path_line_[$name]['duration']  = (strtotime($value['time']['EXIT']) - strtotime($value['time']['ENTER']));
            }
            // print_r($path_line_);
            return $path_line_;
    }

}