<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retail_model extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

	public function get_product($id = ''){
        $sql = $this->db->query("SELECT * FROM saleitems WHERE code = ?", $id);
        
        return $sql->row_array(); 
    }
    
    public function save_master($param = array()){
        $this->db->insert('tr_shop_master', $param);
        return  $this->db->insert_id();
    }

    public function save_detail($param = array()){
        $this->db->insert('tr_shop_detail', $param);
        return  $this->db->insert_id();
    }
}