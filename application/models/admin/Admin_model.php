<?php defined('BASEPATH') OR exit('No direct script access allowed');


class admin_model extends CI_Model{

	function __construct(){

		parent::__construct();
		// $this->load->library('database')
		$this->news = array(
			'title',
			'date',
			'content',
			'img1',
			'img2',
			'img3',
			'entry_stamp'
			);
		$this->procurement = array(
			'title',
			'date',
			'lampiran',
			'entry_stamp'
			);

	}

	/*==============================================
					NEWS & EVENT
	==============================================*/
	function get_news($search='', $sort='', $page='', $per_page='',$is_page=FALSE,$filter=array()){
		$this->db
				->select('ms_news_event.*')
				->where('del', 0)
				->order_by('id', 'DESC');

		if($this->input->get('sort')&&$this->input->get('by')){
			$this->db->order_by($this->input->get('by'), $this->input->get('sort')); 
		}
		if($is_page){
			$cur_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
			$this->db->limit($per_page, $per_page*($cur_page - 1));
		}

		$a = $this->db->group_by('ms_news_event.id');
		$query = $a->get('ms_news_event');
		return $query->result_array();
	}

	function get_per_news($id){
		return $this->db
					->select('*')
					// ->where('ms_news_event.del', 0)
					->where('id', $id)
					->get('ms_news_event')
					->row_array();
	}

	function save_news($data){
		$_param = array();
		$sql = "INSERT INTO ms_news_event (
							title,
							date,
							content,
							img1,
							img2,
							img3,
							entry_stamp
							) 
				VALUES (?,?,?,?,?,?,?) ";
		
		
		foreach($this->news as $_param) $param[$_param] = $data[$_param];
		$this->db->query($sql, $param);
		// print_r($this->db->last_query());
		//

		// print_r($data);die;
	}

	function edit_news($data,$id){
		$this->db->where('id',$id);
		

		$result = $this->db->update('ms_news_event',$data);
		if($result)return $id;
	}

	function delete_news($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_news_event',array('del'=>1));
	}



	/*==============================================
					PROCUREMENT
	==============================================*/
	function get_procurement($search='', $sort='', $page='', $per_page='',$is_page=FALSE,$filter=array()){
		$this->db
				->select('ms_procurement.*')
				->where('del', 0)
				->order_by('id', 'DESC');

		if($this->input->get('sort')&&$this->input->get('by')){
			$this->db->order_by($this->input->get('by'), $this->input->get('sort')); 
		}
		if($is_page){
			$cur_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
			$this->db->limit($per_page, $per_page*($cur_page - 1));
		}

		$a = $this->db->group_by('ms_procurement.id');
		$query = $a->get('ms_procurement');
		return $query->result_array();
	}

	function get_per_procurement($id){
		return $this->db
					->select('*')
					// ->where('ms_news_event.del', 0)
					->where('id', $id)
					->get('ms_procurement')
					->row_array();
	}

	function save_procurement($data){
		$_param = array();
		$sql = "INSERT INTO ms_procurement (
							title,
							date,
							lampiran,
							entry_stamp
							) 
				VALUES (?,?,?,?) ";
		
		
		foreach($this->procurement as $_param) $param[$_param] = $data[$_param];
		$this->db->query($sql, $param);
		// print_r($this->db->last_query());
		//

		// print_r($data);die;
	}

	function edit_procurement($data,$id){
		$this->db->where('id',$id);
		

		$result = $this->db->update('ms_procurement',$data);
		if($result)return $id;
	}

	function delete_procurement($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_procurement',array('del'=>1));
	}

}