<div class="right-panel">

    <div class="row" id="index-first-row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Item</div>
                    <div id="number"><?php echo $item['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Category</div>
                    <div id="number"><?php echo $cat['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Customer</div>
                    <div id="number"><?php echo $cust['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Promo active</div>
                    <div id="number"><?php echo $promo['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
    </div>

    <div class="row">
        <div class="col-12">
            <form>
                <label>Kategori</label>
                <select id="cat">
                    <option selected="true" disabled="true" >Pilih Kategori</option>
                    <option value="makanan">Makanan</option>
                    <option value="minuman">Minuman</option>
                </select>
                <select id="cat_">
                    <option selected="true" disabled="true" >Pilih Kategori</option>
                    <option value="brand">Brand</option>
                    <option value="age">Age</option>
                </select>
                <select id="brand">
                    <option selected="true" disabled="true" >Pilih Brand</option>
                    <option value="orangtua">Orang tua</option>
                    <option value="indofood">indofood</option>
                </select>
            </form>
        </div>
    </div>

    <!-- Chart -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-title" id="chart-title"></div>
                    <div class="chart-frame" id="index-chart"></div>
                </div>
            </div>		
        </div>
    </div>
    
</div>

<script>
        $( document ).ready(function() {
            $("#cat_").hide();
            $("#brand").hide();
            
            $( "#cat" ).on( "change", function() {
                $("#cat_").show();                
            });
            $( "#cat_" ).on( "change", function() {
                console.log( "change" );
                if($( "#cat_" ).val() == 'brand'){
                    $("#brand").show();
                    $("#chart-title").empty();
                    $("#index-chart").empty();
                    $("#chart-title").append("Chart by Brand");

                    Highcharts.chart('index-chart', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            visible: false
                        },
                        yAxis: {
                            min: 0,
                            title: {
                            text: 'Sold Item (pcs)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} pcs</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                            },
                            series: {
                                borderRadius: 5
                            }
                        },
                        series: [{
                            name: 'Arnotts',
                            data: [49.9]

                        }, {
                            name: 'Orang Tua',
                            data: [83.6]

                        }, {
                            name: 'Indofood',
                            data: [48.9]

                        }, {
                            name: 'Wingsfood',
                            data: [42.4]

                        }]
                    });

                }else{
                    $("#brand").hide();
                    $("#chart-title").empty();
                    $("#index-chart").empty();
                    $("#chart-title").append("Chart by Age");

                    Highcharts.chart('index-chart', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [
                            '< 15',
                            '15 - 25',
                            '25 - 35',
                            '35 - 45',
                            '45 - 55',
                            '> 55'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                            text: 'Sold Item (pcs)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} pcs</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                            },
                            series: {
                                borderRadius: 5
                            }
                        },
                        series: [{
                            name: 'Arnotts',
                            data: [49.9, 50, 21, 90, 14]

                        }, {
                            name: 'Orang Tua',
                            data: [83.6, 43, 54, 14, 90]

                        }, {
                            name: 'Indofood',
                            data: [48.9, 80, 80, 40, 30]

                        }, {
                            name: 'Wingsfood',
                            data: [42.4, 10, 20, 30, 40]

                        }]
                    });
                }
            });

            $( "#brand" ).on( "change", function() {
               
                $("#chart-title").empty();
                $("#index-chart").empty();
                $("#chart-title").append("Chart by Brand - Indofood");

                Highcharts.chart('index-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [
                        '< 15',
                        '15 - 25',
                        '25 - 35',
                        '35 - 45',
                        '45 - 55',
                        '> 55'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                        text: 'Sold Item (pcs)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} pcs</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                        },
                        series: {
                            borderRadius: 5
                        }
                    },
                    series: [{
                        name: 'Indomie',
                        data: [49.9, 50, 21, 90, 14]

                    }, {
                        name: 'Popmie',
                        data: [83.6, 43, 54, 14, 90]

                    }, {
                        name: 'Trenz',
                        data: [48.9, 80, 80, 40, 30]

                    }, {
                        name: 'Promina',
                        data: [42.4, 10, 20, 30, 40]

                    }]
                });

            });
        });
</script>
