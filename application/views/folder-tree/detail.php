<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('folder_tree/');?>">Shopping List</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="#">List Member</a>
        </div>
    </div>
    <div class="row" id="folder-tree-first-row">

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="inside-row">
                        <div class="col-12">
                            <div class="header">
                                <div class="page-header-title">
                                    <a href="<?php echo base_url('folder_tree/');?>">List Member</a>
                                    <!-- USER DETAIL -->
                                    <?php if(isset($user)){?>
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="<?php echo base_url('folder_tree/member/'.$user['id']);?>"><?php echo $user['name'];?></a>
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="#"><?php echo date('d M Y', strtotime($date));?></a>
                                    <?php }?>
                                </div>
                                <div class="page-change-view">
                                    <div id="view-as-icon">
                                        <div id="box" class="icon active">
                                            <i class="fas fa-th"></i>
                                        </div>
                                        <div id="list" class="icon">
                                            <i class="fas fa-list"></i>
                                        </div>
                                    </div>
                                </div>	
                                <br class="clear">	
                            </div>
                            <div class="folder-body box-view">
                                <div class="box-title">Shop List</div>
                                <table class="table" cellpadding="0" border="0" cellspacing="0">
                                    <thead>
                                        <tr>

                                            <td width="30%">Item Name</td>
                                            <td>Qty</td>
                                            <td>Category</td>
                                            <td>Store</td>
                                            <td>Location</td>
                                            <td>Timestamp</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($shop as $key => $value){?>
                                        <tr>                                            
                                            <td><?php echo $value['product_name'];?></td>
                                            <td><?php echo $value['qty'];?></td>
                                            <td><?php echo $value['category'];?></td>
					    <td><?php echo $value['store_name'];?></td>
					    <td><?php echo $value['store_location'];?></td>
                                            <td><?php echo date('H:i:s', strtotime($value['entry_stamp']));?></td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>	
                        </div>	
                    </div>
                                            
                    <br class="clear">
                </div>
            </div>
        </div>	
    </div>

</div>