<?php
    
    if(!isset($user)){
        $user['id']     = 0;
        $user['name']   = "non-Member";
    }
?>
<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('folder_tree/');?>">Shopping List</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="<?php echo base_url('folder_tree/');?>">List Member</a>
        </div>
    </div>
    <div class="row" id="folder-tree-first-row">

        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="inside-row">
                        <div class="col-12">
                            <div class="header">
                                <div class="page-header-title">
                                    <a href="<?php echo base_url('folder_tree/');?>">List Member</a>
                                    <!-- USER DETAIL -->
                                    <?php if(isset($user)){?>
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="<?php echo base_url('folder_tree/member/'.$user['id']);?>"><?php echo $user['name'];?></a>
                                    <?php }?>
                                </div>
                                <div class="page-change-view">
                                    <div id="view-as-icon">
                                        <div id="box" class="icon active">
                                            <i class="fas fa-th"></i>
                                        </div>
                                        <div id="list" class="icon">
                                            <i class="fas fa-list"></i>
                                        </div>
                                    </div>
                                </div>	
                                <br class="clear">	
                            </div>
                            <div class="folder-body box-view">
                               
                                <!--MEMBER-->
                                <?php if(isset($member)){?> 
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('folder_tree/member/0/');?>">
                                            <div id="padding">
                                                <div class="icon" id="user"></div>
                                                <div class="title">non-Member</div>
                                            </div>	
                                        </a>	
                                    </li>
                                    <?php foreach($member as $key => $value){?>
                                    <li>
                                        <a href="<?php echo base_url('folder_tree/member/'.$value['id'].'/');?>">
                                            <div id="padding">
                                                <div class="icon" id="user"></div>
                                                <div class="title"><?php echo $value['name'];?></div>
                                            </div>	
                                        </a>	
                                    </li>
                                    <?php }?>
                                </ul>
                                <?php }?>

                                <!--SHOP PER MEMBER-->
                                <?php if(isset($shop)){?> 
                                <?php foreach($shop as $date => $list){?>
                                <div class="page-section">
                                    <div><?php echo date('M', strtotime($date));?></div>
                                </div>
                                
                                <ul>
                                    <?php foreach($list as $date_ => $list_shop){?>
                                    <li>
                                        <a href="<?php echo base_url('folder_tree/member/'.$user['id'].'/'.date('Y-m-d', strtotime($date_)));?>">
                                            <div id="padding">
                                                <div class="icon" id="calendar"></div>
                                                <div class="title"><?php echo date('d/m/Y', strtotime($date_));?></div>
                                            </div>	
                                        </a>	
                                    </li>
                                    <?php }?>
                                </ul>
                                <?php } }?>
                                
                            </div>	
                        </div>	
                    </div>	
                        
                    <br class="clear">
                </div>
            </div>
        </div>	
    </div>

</div>