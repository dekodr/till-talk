<style>

    /* svg text {display: none;} */
    svg g:hover text {display: block;}
    .path {
        stroke-dasharray: 1000;
        stroke-dashoffset: 1000;
        animation: draw 1s linear;
    }
    

    @keyframes draw {
        0% {
            
        }
        50% {
            stroke-dashoffset: 0;
            stroke-opacity: 1;
        }
    }
    @keyframes dash {
        to {
            stroke-dashoffset: 0;
        }
    }
    .mapping{
        transform: scale(1);
    }
    
</style>
<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('/');?>">Dashboard</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="#">Mapping Dekodr Office</a>
        </div>
    </div>
    <div class="row" id="folder-tree-first-row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="inside-row">
                        <div class="col-12">
                            <div class="header">
                                <div class="page-header-title">
                                    <a href="<?php echo base_url('mapping/');?>">Mapping Dekodr Office</a>
                                    <!-- USER DETAIL -->
                                    
                                </div>
                                <br class="clear">	
                            </div>
                            <div class="folder-body box-view mapping">
                              <!-- MAPPING -->
                                <svg width="100%" height="600">
                                    <!-- THE RECTANGLE -->
									<g class="biscuit">
										<rect x="700" y="150" width="250" height="50"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="750" y="175" >BISCUIT</text>
									</g>

									<g class="instant_noodle">
										<rect x="700" y="300" width="250" height="50"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="750" y="325" >INSTANT NOODLE</text>
									</g>

									<g class="drink">
										<rect x="300" y="150" width="250" height="50"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="350" y="175" >DRINK</text>
									</g>
									<g class="snack">
										<rect x="300" y="300" width="250" height="50"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="350" y="325" >SNACK</text>
									</g>
									<g class="instant_food">
										<rect x="300" y="450" width="250" height="50"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="350" y="475" >INSTANT FOOD</text>
									</g>
                                    
									
                                    <!-- PATHFINDER -->
                                    <g id="circuit" class="path" stroke-width="5" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="1" stroke-dasharray="400, 50" stroke-dashoffset="200" fill="none" stroke="#02B3E4">
                                        <path id="circuit_path" d="" stroke=""></path>
                                        <!-- <circle id="car" cx="50" cy="50" fill="#de8787" stroke="#a02c2c"/> -->
                                        <path id="car" d="m42.6667 221.193 20 43.7895-20 43.7895 36.9142-32.9829 36.9142-10.8066-36.9142-10.8066z" fill="#de8787" stroke="#a02c2c"/>
                                    </g>
		  
                                </svg>
                            </div>	
                        </div>	
                    </div>	
					
					
                    <br class="clear">
                </div>
            </div>
        </div>	
    </div>

</div>