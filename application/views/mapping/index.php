<style>

    /* svg text {display: none;} */
    svg g:hover text {display: block;}
    .path {
        stroke-dasharray: 50000;
        stroke-dashoffset: 50000;
        animation: dash 10s linear forwards;
    }

    @keyframes draw {
        0% {
            
        }
        50% {
            stroke-dashoffset: 0;
            stroke-opacity: 1;
        }
    }
    @keyframes dash {
        to {
            stroke-dashoffset: 0;
        }
    }
    .mapping{
        transform: scale(1);
    }
    
</style>
<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('/');?>">Dashboard</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="#">Mapping</a>
        </div>
    </div>
    <div class="row" id="folder-tree-first-row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="inside-row">
                        <div class="col-12">
                            <div class="header">
                                <div class="page-header-title">
                                    <a href="<?php echo base_url('mapping/');?>">Mapping</a>
                                    <!-- USER DETAIL -->
                                    
                                </div>
                                <br class="clear">	
                            </div>
                            <div class="folder-body box-view mapping">
                              <!-- MAPPING -->
                                <?php //THE RECIPE
                                        $x = 0; 
                                        $cordinate = $cor;
                                        // print_r($cor);die;
                                        $aisleX = 50;
                                        $aisleY = 100;
										
										$y = $top = $start = 100;
										$row = $column = 0;
										$height = 75;
										$width = 150;

										$rack = 9;
										$aisle = 6;

										$col_index = array('a','b','c','d','e','f','g','h','i','j','k','l');
										$aisle_array = $product_array = array();
										
										?>
                                <svg width="100%" height="2200">
                                    <!-- THE RECTANGLE -->
                                    <?php foreach ($data_category as $key => $value) {?>
									<g>
										<rect id="<?php echo $value['sub_category'];?>" x="<?php echo $x;?>" y="<?php echo $y;?>" width="50" height="75"  style="fill:rgb(223, 230, 233);stroke-width:3;stroke:rgb(99, 110, 114)" />
										<text x="<?php echo $x;?>" y="<?php echo ($y + 20);?>">
											<?php echo $key; //echo $value['sub_category'];?>
										</text>
									</g>
									
                                    <?php
										array_push($product_array, array($row.$col_index[$column], $value['sub_category'], ($x + 90), ($y + 32)));
										
										$y += $height;
										if(($key + 1) % $rack == 0 and $key){
											
											$enterance = "";
											if(!$row) $enterance = "entrance";
											else $enterance = ($row - 1).$col_index[$column]; 	
											
											$xPos = (150 * $column) + 90; 
											$yPos = ((($height * $rack) + 75) * $row) + 50;
											
											array_push($aisle_array, array($enterance, $row.$col_index[$column], $xPos, $yPos));
											
											$x += $width;
											$y = $top;
											$column++;									
										}
										
										if($column == $aisle){
											$row++;
											
											$y = $top = ((($height * $rack) + 75) * $row) + $start;
											$x = 0;
											$column = 0;
										}										
									}?>
                                    <!-- PATHFINDER -->
                                    <g id="circuit" class="path" stroke-width="5" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="1" stroke-dasharray="400, 50" stroke-dashoffset="200" fill="none" stroke="#02B3E4">
                                        <path id="circuit_path" d="" stroke=""></path>
                                        <path id="car" d="m42.6667 221.193 20 43.7895-20 43.7895 36.9142-32.9829 36.9142-10.8066-36.9142-10.8066z" fill="#de8787" stroke="#a02c2c"/>
                                    </g>
		  
                                </svg>
                            </div>	
                        </div>	
                    </div>	
					
					<script type="text/javascript">
					<?php 
						$final_map = array();
						
						foreach($aisle_array as $data) array_push($final_map, $data);
						foreach($product_array as $data) array_push($final_map, $data);
							
						echo "path_file = [";
						$var = "";
							foreach($final_map as $data){
								$var .= '["'.$data[0].'","'.$data[1].'",'.$data[2].','.$data[3].'],';
							}
							
						echo rtrim($var, ",");
						echo "];";
					?>
					</script>
					
                    <br class="clear">
                </div>
            </div>
        </div>	
    </div>

</div>