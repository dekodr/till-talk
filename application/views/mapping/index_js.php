<script> 


    /*
    *
    ***************************************************************************************************************
    *       ITEM LIBRARY PATH
    ***************************************************************************************************************
    *
    */
		
		/*
		// PATH DATA 
        path_file = [
            
            // [FROM, TARGET, X, 50]
            
			["x9", "CREAM SOAP", 100, 850],
            // ["CREAM SOAP", "x8_", 250, 850],
            ["x8_", "DISHWASHER SOAP", 250, 950],
            ["x6_", "GLASS CLEANER", 550, 950],
            ["x8_", "COCONUT MILK", 250, 1350],
            ["x5_", "SALT", 700, 1350],
            ["x3_", "CAMPHOR", 1000, 770],
			
            // 1st ROW AISLE
            ["entrance", "x1", 1130, 50],
            ["entrance", "x2", 1030, 50],
            ["entrance", "x3", 1000, 50],
            ["entrance", "x4", 850, 50],
            ["entrance", "x5", 700, 50],
            ["entrance", "x6", 550, 50],
            ["entrance", "x7", 400, 50],
            ["entrance", "x8", 250, 50],
            ["entrance", "x9", 100, 50],

            // 2nd ROW AISLE
            ["x1", "x1_", 1130, 850],
            ["x2", "x2_", 1030, 850],
            ["x3", "x3_", 1000, 850],
            ["x4", "x4_", 850, 850],
            ["x5", "x5_", 700, 850],
            ["x6", "x6_", 550, 850],
            ["x7", "x7_", 400, 850],
            ["x8", "x8_", 250, 850],
            ["x9", "x9_", 100, 850],

            // 3nd ROW AISLE
            ["x1", "x1_", 1130, 1850],
            ["x2", "x2_", 1030, 1850],
            ["x3", "x3_", 1000, 1850],
            ["x4", "x4_", 850, 1850],
            ["x5", "x5_", 700, 1850],
            ["x6", "x6_", 550, 1850],
            ["x7", "x7_", 400, 1850],
            ["x8", "x8_", 250, 1850],
            ["x9", "x9_", 100, 1850],

            // ITEM CORDINATE
            ["x4", "KRETEK CIGARETES", 850, 450],
            ["x6", "JAM", 550, 450],
            // ["x6", "GLASS CLEANER", 550, 950],
            ["x5", "CIGARETES", 700, 450],


        ];
		*/
        
        var target_ = "<?php echo $cor;?>";//"WASH EQUIPMENT,WOMAN SHAMPOO"; 
            target_ = target_.split(",");
        var path    = [];

        console.log(target_);
        

        for (var i = 0; i < target_.length; i++) {
            // If we only have 1 parameter or else we had more than 1s
            if (i == 0) {
                console.log("entrance - "+target_[i]);
                path += dijkstra(path_file, "entrance", target_[i]);
                // path += getPath(path_file, "entrance", target_[0]);
            }else{
                console.log(target_[i - 1]+" - "+target_[i]);
                path = getPath(path_file, target_[i], path);
            }
            
        }
        
        //path ="entrance,BIKE SHAMPOO";
        path = path.split(",");

        console.log("path : "+path);
        
        // DESCRIBE THE CORDINATE LINE 
        var cor_line = "M 0 50";

    /***************************************************************************************************************/




    
    /*************************************************************************************************************** 
     *************************************************************************************************************** 
     *         APPLY PATH TO CORDINATE LINE                                                                        * 
     *************************************************************************************************************** 
     ***************************************************************************************************************/
    path.forEach(function(element) {
        
        // if(element !== "entrance"){
            var cor     = getIndexOfK(path_file, element);
            if(cor !== undefined){

                var corX    = path_file[cor][2];
                var corY    = path_file[cor][3];

                cor_line += " L "+corX+" "+corY;
            }   
        // }
    });

    // Implement cordinate to path
    console.log(cor_line);
    $("#circuit_path").attr("d", cor_line);


    /***************************************************************************************************************/

















    /*
    ************************************
    *       DIJKSTRA  FUNCTION
    ************************************
    */
    function dijkstra( path_file, source, target){

        //buildis adjacency list
        var vertices = [];
        var neighbours = {};

        path_file.forEach( function( edge ){
            //store the vertex 0
            if( vertices.indexOf( edge[2] ) == -1 )vertices.push( edge[0] );
            //creates the adjacency map for this vertex
            if( neighbours[edge[0]] == null )neighbours[edge[0]] = [];
            //stores the other vertex of this edges in the adjacency map
            neighbours[edge[0]].push( { end: edge[1], cost: edge[2]} );

            //same for the other vertex of this edge
            if( vertices.indexOf( edge[1] ) == -1 )vertices.push( edge[1] );
            if( neighbours[edge[1]] == null )neighbours[edge[1]] = [];
            neighbours[edge[1]].push( { end: edge[0], cost: edge[2]} );

        });


        //creates a distance dictionary & an linked list (to store the shortest path )
        var dist = {};
        var previous = {};
        //initializes them with default values
        vertices.forEach( function( vertex){
            dist[ vertex ] = Number.POSITIVE_INFINITY;
            previous[vertex] = null;
        } );

        //initializes the distance to source at 0
        dist[source] = 0;
        var queue = vertices;
        while( queue.length > 0 ){

            //select the closest vertex
            min = Number.POSITIVE_INFINITY;
            queue.forEach( function(vertex){
                if (dist[vertex] < min){
                    min = dist[ vertex ];
                    u = vertex;
                }
            });
            
            //removes current node from the queue
            queue.splice( queue.indexOf( u ), 1 );

            if( dist[ u ] == Number.POSITIVE_INFINITY || u == target ){
                break;
            }

            //for all neighbours of U, update distance to target node
            if( neighbours[ u ] != null ){
                neighbours[ u ].forEach(
                    function(arr){
                        var alt = dist[u] + arr[ "cost" ];
                        if( alt < dist[ arr[ "end" ]]){
                            dist[ arr[ "end" ]] = alt;
                            previous[ arr[ "end" ]] = u;
                        }
                    });
            }
        }

        //compute shortest path
        var solution = [];
        while( previous[ target ] != null ){
            solution.unshift( target );
            target = previous[ target ];
        }
        solution.unshift( target );
        return solution;
    }



    /*
    ************************************
    *       OTHER  FUNCTION
    ************************************
    */
    
    function getIndexOfK(arr, k) {
        for (var i = 0; i < arr.length; i++) {
            var index = arr[i].indexOf(k);
            if (index > -1) {
                return i;
            }
        }
    }


    function getPath(arr, k, path) {

        for (var i = 0; i < arr.length; i++) {
            var index = arr[i].indexOf(k);
            if (index > -1) {
                
                var aisle = arr[i][0];
                break;
            }
        }

        path += ","+dijkstra(path_file, aisle, k);
        path += ","+dijkstra(path_file, k, aisle);
        // console.log(">>"+dijkstra(path_file, aisle, k));
        return path;
    }


    // --------------------------------------------
    window.addEventListener("load", function() {
    var circuit_path = document.getElementById("circuit_path");
    var car = document.getElementById("car");
    circuit_pathLength=circuit_path.getTotalLength() 
    circuit_path.setAttribute("stroke-dasharray",circuit_pathLength);
    circuit_path.setAttribute("stroke-dashoffset",circuit_pathLength)
    animStart=true;
    loop=true;
    var delta = 40;

    var start = Date.now();
    var f=0;
    var pause=3000;
    //console.log(f);
    steps= 3000;
    function animation() {
        if (animStart == true){
            dLength=circuit_pathLength/steps;
            point=circuit_path.getPointAtLength(f*dLength);
            pointb=circuit_path.getPointAtLength((f+0.1)*dLength);
            //console.log (f*dLength/circuit_pathLength, point.x, point.y);
            p1=point;
            p2=pointb;
            angle=Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
            carBBox =car.getBBox();
            console.log(carBBox);
            cx=carBBox.x+carBBox.width/2;
            cy=carBBox.y+carBBox.height/2 ;
            circuit_path.setAttribute("stroke-dashoffset", circuit_pathLength-f*dLength);
            transString = " translate(" + Number(point.x - carBBox.x-carBBox.width/2) + ", "+ Number(point.y - carBBox.y-carBBox.height/2) +")";
            rotateString = " rotate("+ angle + " " + cx + " " + cy +")"; 
            stringTransform = transString + rotateString 
            car.setAttribute("transform", stringTransform)
            if(f>= steps -1) {
                animStart = false;
                f=0;
            }

        }

        if(Date.now()-start>delta){
            start=Date.now();
            f++;
        }
        if(loop==true) {
            window.requestAnimationFrame(animation);
        }
    }
    window.requestAnimationFrame(animation);	
}, false);	


</script>