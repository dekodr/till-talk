<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('member');?>">Member</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="<?php echo base_url('member/detail/'.$user['id']);?>">Member Detail</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a>Total Category</a>
        </div>
    </div>
    <div class="row" id="header-title-only">

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Total Brands data</div>
                    <div class="image-icon header online-shop size-md"></div>
                    <br class="clear">
                </div>
            </div>
        </div>         
    </div>
    <div class="row brands-detail member-detail-page" >
        
        <div class="col-5">
            <div class="card">
                <div class="card-body"> 
                    <div class="box-title">Brands Statistic</div>
                    <div class="purchase-statistic">
                        <ul>
                            <li>
                                <div class="title">Top Brand</div>
                                <span class="sub-title">Ultra Milk UHT 250 Ml</span>
                            </li>
                            <li>
                                <div class="title">Total Sales</div>
                                <span class="sub-title">68.902.300</span>
                            </li>
                            <li>
                                <div class="title">Brands Promo</div>
                                <span class="sub-title">18</span>
                            </li>
                            <li>
                                <div class="title">Brands Loyalty Averages</div>
                                <span class="sub-title">42%</span>
                                <i class="fas fa-chevron-circle-up up"></i>
                            </li>
                            <li>
                                <div class="title">Orders Per week</div>
                                <span class="sub-title">87</span>
                                <i class="fas fa-chevron-circle-up up"></i>
                            </li>
                        </ul>    
                    </div>  
                </div>
            </div>
        </div>
         <div class="col-7">
            <div class="card">
                <div class="card-body"> 
                    <div class="box-title">Category Brands</div>
                    <div class="chart-frame" id="chart-by-brands"></div>
                </div>
            </div>
        </div>
    </div>  
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Brands info by category</div>
                    <table class="table table-2" cellpadding="0" border="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td width="35%">Brand</td>
                                <td width="">Category</td>
                                <td width="">Purchases</td>
                                <td width="">Status</td>
                                <td width="">Loyalty</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $detail = usort($analitycs['brands-info-by-category']); 
                                var_dump($detail);
                            ?>
                            <?php foreach ($detail as $key => $value) {?>
                                <tr>
                                    <td><?php echo $value['top-brand'];?></td>
                                    <td><?php echo $value['category'];?></td>
                                    <td><?php echo $value['purchses'];?></td>
                                    <td><div class="badge badge-<?php echo ($value['status'] == "undecided") ? "warning" : "success" ;?>"><?php echo $value['status'];?></div></td>
                                    <td><?php echo $value['loyalty'];?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    </div>
                <br class="clear">
                </div>
            </div>
        </div>
    </div>  
</div>

<script>

    jQuery(document).ready(function($){
        // Monthly Average Revenue
        Highcharts.chart('chart-by-brands', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height: 450
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                innerSize: '60%',
                colorByPoint: true,
                data: [

                <?php foreach ($data_category as $key => $value) {?>
                {
                    name: '<?php echo $value['name']?>',
                    y: <?php echo $value['y']?>,
                },
                <?php }?>]
            }]
        });


        // THEME OPTION
        /**
         * (c) 2010-2018 Torstein Honsi
         *
         * License: www.highcharts.com/license
         *
         * Grid-light theme for Highcharts JS
         * @author Torstein Honsi
         */

        'use strict';
        // import Highcharts from '../parts/Globals.js';
        /* global document */
        // Load the fonts
        Highcharts.createElement('link', {
            href: 'https://fonts.googleapis.com/css?family=Dosis:400,600',
            rel: 'stylesheet',
            type: 'text/css'
        }, null, document.getElementsByTagName('head')[0]);

        Highcharts.theme = {
            colors: ['#7cb5ec', '#f7a35c', '#90ee7e', '#7798BF', '#aaeeee', '#ff0066',
                '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                backgroundColor: null,
                style: {
                    fontFamily: 'Dosis, sans-serif'
                }
            },
            title: {
                style: {
                    fontSize: '16px',
                    fontWeight: 'bold',
                    textTransform: 'uppercase'
                }
            },
            tooltip: {
                borderWidth: 0,
                backgroundColor: 'rgba(219,219,216,0.8)',
                shadow: false
            },
            legend: {
                itemStyle: {
                    fontWeight: 'bold',
                    fontSize: '13px'
                }
            },
            xAxis: {
                gridLineWidth: 1,
                labels: {
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            yAxis: {
                minorTickInterval: 'auto',
                title: {
                    style: {
                        textTransform: 'uppercase'
                    }
                },
                labels: {
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            plotOptions: {
                candlestick: {
                    lineColor: '#404048'
                }
            },


            // General
            background2: '#F0F0EA'

        };

        // Apply the theme
        Highcharts.setOptions(Highcharts.theme);
    });
</script>
