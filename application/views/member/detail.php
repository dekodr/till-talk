<div class="right-panel">
    <div class="page-info-frame">
        <div class="body">
            <a href="<?php echo base_url('member');?>">Member</a>
            <i class="fas fa-chevron-circle-right"></i>
            <a href="<?php echo base_url('member/'.$user['id']);?>">Member Detail</a>
        </div>
    </div>
    <div class="row" id="member-first-row">

        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="inside-row">
                        <div class="col-12">
                            <div class="box-title"><?php echo $user['name'];?></div>
                        </div>	
                    </div>	
                        
                    <div class="inside-row">
                        <div class="col-7">
                            <table class="desc-table" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>Member ID</td>
                                    <td><?php echo $user['member_id'];?></td>
                                </tr>
                                <tr>
                                    <td>Card Type</td>
                                    <td><?php echo $user['card_type'];?></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td><?php echo $user['birth_date'];?></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td><?php echo $user['address'];?></td>
                                </tr>
                                <tr>
                                    <td>Occupation</td>
                                    <td>Employee</td>
                                </tr>
                            </table>
                            <table class="desc-table bottom" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>Card Number</td>
                                    <td><?php echo $user['card_number'];?></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td class="green">Active</td>
                                </tr>
                                <tr>
                                    <td>Total Point</td>
                                    <td>1.028</td>
                                </tr>
                                <tr>
                                    <td>Customer-Summary</td>
                                    <td>Undecided buyer, can target any promo</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-5">
                            <div class="card-frame">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <div class="card-box" id="platinum">
                                                <div id="info">
                                                    <div id="card-number"><?php echo $user['card_number'];?></div>
                                                    <div id="card-name"><?php echo $user['name'];?></div>
                                                </div>	
                                            </div>
                                        </div>
                                        <div class="back">
                                            <div class="card-box" id="platinum-back">
                                                <div id="qr-code">
                                                    <img src="<?php echo base_url('/asset/images/qr/'.$user['qr'])?>">
                                                </div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="edit-frame">
                                    <button class="btn btn-primary">
                                        <i class="fas fa-edit"></i>
                                        edit
                                    </button>
                                    <button class="btn btn-danger">
                                        <i class="fas fa-lock"></i>
                                        deactive
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>	
                    <br class="clear">
                </div>
            </div>
        </div>	
    </div>

    <div class="row" id="member-2-row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Loyalty Score</div>
                    <div id="number"> 61</div>
                    <div id="icon"></div>
                    <div class="btn btn-primary btn-fw">See Data</div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <a href="<?php echo base_url('member/category/'.$user['id']);?>">   
                <div class="card">
                    <div class="card-body">
                        <div id="title">Total Brands</div>
                        <div id="number"><?php echo $data['cat'];?></div>
                        <div id="icon"></div>
                        <div class="btn btn-primary btn-fw">See Data</div>
                    </div>
                </div>
            </a> 
        </div>
        <div class="col-3">
	    <a href="<?php echo base_url('folder_tree/member/'.$user['id']);?>">
            <div class="card">
                <div class="card-body">
                    <div id="title">Purchase History</div>
                    <div id="number"><?php echo $data['shops'];?></div>
                    <div id="icon"></div>
                    <div class="btn btn-primary btn-fw">See Data</div>
                </div>
            </div>
	    </a>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Promo Active</div>
                    <div id="number">4</div>
                    <div id="icon"></div>
                    <div class="btn btn-primary btn-fw">See Data</div>
                </div>
            </div>
        </div>  	
    </div>
    <div class="row member-detail-page">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Top Item bought</div>
                    <table class="table" cellpadding="0" border="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td width="60%">Item Name</td>
                                <td width="20%">Total purchase</td>
                                <td>Loyalty</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Chitato 165gr</td>
                                <td>16</td>
                                <td>74%</td>
                            </tr>
                            <tr>
                                <td>Coca Cola, 3 liter</td>
                                <td>15</td>
                                <td>70%</td>
                            </tr>
                            <tr>
                                <td>Monde Serena 55gr</td>
                                <td>11</td>
                                <td>64%</td>
                            </tr>
                            <tr>
                                <td>Stella MT FLW ref225</td>
                                <td>9</td>
                                <td>52%</td>
                            </tr>
                            <tr>
                                <td>Indomie Ayam Bawang</td>
                                <td>9</td>
                                <td>45%</td>
                            </tr>
                            <tr>
                                <td>Chitato 165gr</td>
                                <td>8</td>
                                <td>43%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div> 
         <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Purchase Statistic</div>
                    <div class="purchase-statistic">
                        <ul>
                            <li>
                                <div class="title">Total Purchase</div>
                                <span class="sub-title">Rp 12.873.300</span>
                            </li>
                            <li>
                                <div class="title">Average Purchase</div>
                                <span class="sub-title">Rp 320.400</span>
                                <i class="fas fa-chevron-circle-up up"></i>
                            </li>
                            <li>
                                <div class="title">Purchase with Promo</div>
                                <span class="sub-title">Rp 1.400.000</span>
                                <i class="fas fa-chevron-circle-down down"></i>
                            </li>
                        </ul>    
                    </div>  
                </div> 
            </div>
        </div>        
    </div>
</div>