<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>TillTalk - POS</title>
		<link rel="shortcut icon" type="image" href="<?php echo base_url(); ?>asset/images/tilltalk-icon.png">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/pos-style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/component.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">	
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-tilltalk-master.js"></script>
	</head>
	<body>
		<div class="master-body" id="pos-index">
			<div id="logo"></div>
			<div class="center-frame">
				<div id="left">
					<img src="asset/images/pos-index-image1.jpg">
				</div>
				<div id="right">
					<div class="inner-frame">
						<div id="toko-title">
							Toko <span>Makmur Sejahtera</span>
						</div>	
						<a href="<?php echo base_url()."retail/transaction"; ?>">
							<div id="button">
								<div id="icon">
									<div class="anim-box" id="one"></div>
									<div class="anim-box" id="two"></div>
									<div class="anim-box" id="three"></div>
									<img src="asset/images/pos-icon-cart.png">
								</div>	
								Mulai Belanja
							</div>
						</a>	
					</div>	
				</div>
			</div>
		</div>
	</body>
</html>

