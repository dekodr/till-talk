
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>TillTalk - POS</title>
		<link rel="shortcut icon" type="image" href="<?php echo base_url(); ?>asset/images/tilltalk-icon.png">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/pos-style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/component.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/animation.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/keyboard-dark.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">	
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-ui.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery.keyboard.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-tilltalk-master.js"></script>
		<script type="text/javascript">
			$( document ).ready( function(){
				$('#barcode').keyboard({

				  language     : null, // string or array
				  layout: 'custom',
				  alwaysOpen   : true,
				  accepted : function(event, keyboard, el) {
    					$("#form").submit();
  				  },
				  customLayout : {
				    'normal': [
				      '7 8 9',
				      '4 5 6',
				      '1 2 3',
				      '0 {bksp} {a}'
				    ]
				  }

				});

				});

 	
		</script>
	</head>
	<body>
		<div class="master-body" id="pos-dashboard">
			<div class="center-frame right-panel">
				<div class="row" id="pos-dashboard-first-row">
					<div class="col-12">

						<div id="store-title">
							Toko <span>Makmur Sejahtera</span>
						</div>
						<div id="logo"></div>

					</div>	
				</div>
				<div class="row" id="pos-dashboard-second-row">
					<div class="col-7">
						<div class="card">
							<div class="card-body">
								<div class="box-title">Total Pembelian</div>
								<div id="total-price">
									<?php 
										foreach($session as $data) $total += $data['price']; 
										echo number_format($total);
									?>

								</div>
								<table class="table" cellpadding="0" border="0" cellspacing="0">
									<thead>
										<tr>
											<td width="50%">Nama item</td>
											<td width="25%">Jumlah</td>
											<td>Harga</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($session as $data){ ?>
                                    <tr>
                                        <td><?php echo $data['name']; ?></td>
                                        <td><?php echo $data['qty']; ?></td>
					<td><?php echo number_format($data['price']); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    <td>
                                    <?php } ?>									</tbody>
								</table>

							</div>
						</div>
					</div>
					<div class="col-5">
						<div class="card">
							<div class="card-body">
								<form id="form" method="post">
								<div class="box-title">Barcode</div>
								<input id="barcode" type="number" name="barcode" placeholder="masukan disini" >
								</form>

								<div class="button-frame">
									<button class="btn btn-success" onclick="location.replace('<?php echo base_url()."retail/finish"; ?>')">
										<i class="fas fa-check-circle"></i><br>
										selesai transaksi
									</button>
										<button class="btn btn-danger" onclick="location.replace('<?php echo base_url()."retail/batal"; ?>')">
											<i class="fas fa-times-circle"></i><br>
											Batal
										</button>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
			<div class="footer-frame">

			</div>	
		</div>
	</body>
</html>


