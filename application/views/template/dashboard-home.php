<div class="right-panel">

    <div class="row" id="index-first-row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Item</div>
                    <div id="number"><?php echo $item['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Category</div>
                    <div id="number"><?php echo $cat['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Customer</div>
                    <div id="number"><?php echo $cust['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div id="title">Promo active</div>
                    <div id="number"><?php echo $promo['total'];?></div>
                    <div id="icon"></div>
                </div>
            </div>
        </div>	
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Monthly Average Revenue</div>
                    <div class="chart-frame" id="index-chart-1"></div>
                </div>
            </div>		
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Most Purchased</div>
                    <div class="chart-frame" id="index-chart-2"></div>
                </div>
            </div>		
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Top List</div>
                    <table class="table" cellpadding="0" border="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td width="50%">Item Name</td>
                                <td width="25%">Sales</td>
                                <td>Category</td>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- TOP LIST SECTION -->
                            <?php foreach($top_list as $key => $value){?>
                            <tr>
                                <td><?php echo $value['product_name'];?></td>
                                <td><?php echo $value['total'];?></td>
                                <td><?php echo $value['sub_category'];?></td>
                            </tr>
                            <?php }?>
                            <!-- END OF TOP LIST SECTION -->
                        </tbody>
                    </table>
                </div>
            </div>		
        </div>	
    </div>	
</div>