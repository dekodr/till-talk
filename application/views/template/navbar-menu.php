<div class="navbar-menu">
	<div id="logo"></div>

	<div class="navbar-right">
		<div id="search-bar-frame">
			<div id="search-bar">
				<input id="search-fill" type="text" name="textfield1" maxlength="80" 
				placeholder="Search ..." / required>
				<button type="submit" value="search" id="search-button">
					<i class="fas fa-search"></i>
				</button>
			</div>	
		</div>
		<div id="side-menu-trigger">
			<button class="cd-nav-trigger">
				<span aria-hidden="true"></span>
			</button>
		</div>	
	</div>	

	<div class="side-menu-body">
    
	</div>
</div>