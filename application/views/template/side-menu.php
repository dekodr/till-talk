<ul id="side-menu">
    <li class="<?php if(uri_string()==""){echo "active";} ?>">
        <a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a>
    </li>
    <!-- <li>
        <a href="#"> <i class="far fa-calendar-alt"></i> Daily</a>
    </li> -->
    <li class="<?php if(uri_string()=="member"){echo "active";} ?>">
        <a href="<?php echo base_url('member');?>   "><i class="fas fa-users"></i> Member</a>
    </li>
    <!-- <li>
        <a href="product.php"><i class="fas fa-barcode"></i> Product</a>
    </li> -->
    <!-- <li>
        <a href="category.php"><i class="fas fa-list-ul"></i> Category</a>
    </li> -->
    <li class="<?php if(uri_string()=="folder_tree"){echo "active";} ?>">
        <a href="<?php echo base_url('folder_tree');?>"><i class="fas fa-sitemap"></i> Shoping List</a>
    </li>
    <li class="<?php if(uri_string()=="mapping/member"){echo "active";} ?>">
        <a href="<?php echo base_url('mapping/member');?>"><i class="fas fa-map"></i> Mapping</a>
    </li>
</ul>