<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $title;?></title>
		<link rel="shortcut icon" type="image" href="<?php echo base_url(); ?>asset/images/tilltalk-icon.png">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/component.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/component-ui.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">	
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-tilltalk-master.js"></script>
		<script type="text/javascript">
			$(function() {
				var $window = $(window),
					$sidebar = $(".left-panel #side-menu"),
					sidebarTop = $sidebar.position().top,
					sidebarHeight = $sidebar.height(),
					$footer = $(".side-menu-fixed-end"),
					footerTop = $footer.position().top;

				$sidebar.css('position', 'fixed');
				$window.scroll(function(e) {
					scrollTop = $window.scrollTop();
					topPosition = Math.max(60, sidebarTop - scrollTop);
					topPosition = Math.min(topPosition, (footerTop - scrollTop) - sidebarHeight);
					$sidebar.css('top', topPosition);
				});
			});
		</script>
	</head>
	
	<body>
		<div id="master-body">

			<div class="header-frame"></div>
		    <?php echo $navbarMenu; ?>

			<div class="master-frame">
				<div class="left-panel">
					<?php echo $userInfo; ?>
					<?php echo $sideMenu; ?>
				</div>
                
                <!-- PAGE CONTENT -->
                <?php echo $contentMenu; ?>
				<br class="clear">
			</div>

			<div class="footer-frame">

			</div>	
			<div class="side-menu-fixed-end"></div>
		</div>
		<!-- <script src="<?php #echo base_url(); ?>asset/js/tilltalk-highcharts-index.js"></script> -->
        <?php echo $script; ?>
	</body>
</html>