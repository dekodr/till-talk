$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 30) {
        $(".navbar-menu").addClass("scroll-active");
    } else {
        $(".navbar-menu").removeClass("scroll-active");
    }
});


$( document ).ready( function(){
	$('.header .page-change-view .icon#list').click(function(){
	  $(this).addClass('active');
	  $('.header .page-change-view .icon#box').removeClass('active');
	  $('.row#folder-tree-first-row .folder-body').removeClass('box-view');
	  setTimeout(function(){
	       $('.row#folder-tree-first-row .folder-body').addClass('list-view');
	   }, 0);
	});

	$('.header .page-change-view .icon#box').click(function(){
	  $(this).addClass('active');
	  $('.header .page-change-view .icon#list').removeClass('active');
	  $('.row#folder-tree-first-row .folder-body').removeClass('list-view');
	  setTimeout(function(){
	       $('.row#folder-tree-first-row .folder-body').addClass('box-view');
	   }, 0);
	});

});

