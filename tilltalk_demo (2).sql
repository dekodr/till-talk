-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 30, 2018 at 05:51 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tilltalk_demo`
--
CREATE DATABASE IF NOT EXISTS `tilltalk_demo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tilltalk_demo`;

-- --------------------------------------------------------

--
-- Table structure for table `ms_beacon`
--

DROP TABLE IF EXISTS `ms_beacon`;
CREATE TABLE `ms_beacon` (
  `id` int(11) NOT NULL,
  `id_store` int(11) DEFAULT NULL,
  `sub_category` varchar(100) DEFAULT NULL,
  `beacon_id` varchar(45) DEFAULT NULL,
  `minor` varchar(100) DEFAULT NULL,
  `major` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_beacon`
--

INSERT INTO `ms_beacon` (`id`, `id_store`, `sub_category`, `beacon_id`, `minor`, `major`) VALUES
(1, 1, 'DRINK', NULL, '1126', ''),
(2, 1, 'SNACK', NULL, '1127', ''),
(3, 1, 'INSTANT FOOD', NULL, '1128', '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_beacon_path`
--

DROP TABLE IF EXISTS `tr_beacon_path`;
CREATE TABLE `tr_beacon_path` (
  `id` int(11) NOT NULL,
  `id_beacon` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_shop_master` int(11) NOT NULL DEFAULT '9999',
  `status` varchar(50) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `del` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_beacon_path`
--

INSERT INTO `tr_beacon_path` (`id`, `id_beacon`, `id_member`, `id_shop_master`, `status`, `time`, `del`) VALUES
(1, 1, 1, 9999, 'ENTER', '2018-10-30 01:00:00', 0),
(2, 1, 1, 9999, 'EXIT', '2018-10-30 01:00:10', 0),
(3, 2, 1, 9999, 'ENTER', '2018-10-30 01:01:10', 0),
(4, 2, 1, 9999, 'EXIT', '2018-10-30 01:01:31', 0),
(8, 3, 1, 9999, 'ENTER', '2018-10-30 01:02:04', 0),
(9, 3, 1, 9999, 'EXIT', '2018-10-30 01:02:14', 0),
(10, 3, 2, 9999, 'ENTER', '2018-10-30 01:00:00', 0),
(11, 3, 2, 9999, 'EXIT', '2018-10-30 01:00:10', 0),
(12, 1, 3, 9999, 'ENTER', '2018-10-30 01:01:10', 0),
(13, 1, 3, 9999, 'EXIT', '2018-10-30 01:01:31', 0),
(14, 2, 3, 9999, 'ENTER', '2018-10-30 01:02:04', 0),
(15, 2, 3, 9999, 'EXIT', '2018-10-30 01:02:14', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_beacon`
--
ALTER TABLE `ms_beacon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_store` (`id_store`),
  ADD KEY `beacon_id` (`beacon_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tr_beacon_path`
--
ALTER TABLE `tr_beacon_path`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_beacon`
--
ALTER TABLE `ms_beacon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tr_beacon_path`
--
ALTER TABLE `tr_beacon_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
